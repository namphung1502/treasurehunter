import * as PIXI from 'pixi.js'
import Scene from './Scene.js'
import Object from './Object.js'
import Explorer from './Explorer.js';
import HealthBar from './HealthBar.js';
import { contain, randomInt, hitTestRectangle } from './CommonFunction.js';
const Loader = PIXI.Loader.shared,
    Application = PIXI.Application,
    resources = PIXI.Loader.shared.resources;

class Game extends Application {
    constructor() {
        super();
        this.width = 512;
        this.height = 512;
        this.level = 1;
        document.body.appendChild(this.view);
        this.renderer.resize(this.width, this.height);
        this.renderer.backgroundColor = 0x333333;
    }
    init() {
        Loader.add("image/treasureHunter.json")
            .add("button", "image/button.jpg")
            .load(this.setup.bind(this))
    }
    setup() {
        this.gameScene = new Scene();
        this.stage.addChild(this.gameScene);

        this.dungeon = new Object(resources["image/treasureHunter.json"].textures["dungeon.png"]);
        this.stage.addChild(this.dungeon);
        this.gameScene.addChild(this.dungeon);

        this.door = new Object(resources["image/treasureHunter.json"].textures["door.png"]);
        this.door.setPosition(32, 0);
        this.gameScene.addChild(this.door);

        this.explorer = new Explorer(resources["image/treasureHunter.json"].textures["explorer.png"]);
        this.explorer.setPosition(68, this.height / 2 - this.explorer.height / 2);
        this.explorer.setSpeed(0, 0);
        this.explorer.move();
        this.gameScene.addChild(this.explorer);

        this.treasure = new Object(resources["image/treasureHunter.json"].textures["treasure.png"]);
        this.treasure.setPosition(this.width - this.treasure.width - 48, this.height / 2 - this.treasure.height / 2);
        this.gameScene.addChild(this.treasure);

        this.healthBar = new HealthBar();
        this.healthBar.setPosition(this.width - 170, 4);
        this.gameScene.addChild(this.healthBar);

        this.setupBlobs();
        this.drawTextLevel()

        this.gameOverScene = new Scene();
        this.stage.addChild(this.gameOverScene);
        this.drawEndScene();

        this.nextLevelGameScene = new Scene();
        this.stage.addChild(this.nextLevelGameScene);
        this.drawNextLevelScene();

        this.state = this.play;
        this.ticker.add((delta) => this.loop(delta));
    }

    loop(delta) {
        this.state(delta);
    }

    play(delta) {
        let increSpeed = 2,
            decreBlood = 2;
        this.explorer.x += this.explorer.vx;
        this.explorer.y += this.explorer.vy;

        contain(this.explorer)

        for (let i = 0; i < this.blobs.length; i++) {
            if (this.level == 2) {
                increSpeed = 4;
                decreBlood = 4;

            } else if (this.level == 3) {
                increSpeed = 8;
                decreBlood = 6;
            }
            if (this.blobs[i].vy > 0) {
                this.blobs[i].vy = increSpeed;
            } else {
                this.blobs[i].vy = -increSpeed;
            }
            this.levelText.text = "Level " + this.level;
            this.blobs[i].y += this.blobs[i].vy;

            const blobHitsWall = contain(this.blobs[i]);

            if (blobHitsWall === "top" || blobHitsWall === "bottom") {
                this.blobs[i].vy *= -1;
            }
            if (hitTestRectangle(this.explorer, this.blobs[i])) {
                this.explorer.alpha = 0.5;
                this.healthBar.decreaseBlood(decreBlood);
            } else {
                this.explorer.alpha = 1
            }
        }

        if (hitTestRectangle(this.explorer, this.treasure)) {
            this.treasure.x = this.explorer.x + 8;
            this.treasure.y = this.explorer.y + 8;
        }

        if (this.healthBar.outer.width < 0) {
            this.state = this.end;
            this.message.text = "You lost!";
        }

        if (hitTestRectangle(this.treasure, this.door)) {
            if (this.level === 3) {
                this.state = this.end;
                this.message.text = "You won!";
            } else {
                this.gameScene.setVisible(false);
                this.nextLevelGameScene.setVisible(true);

            }
        }
    }

    end() {
        this.gameScene.setVisible(false);
        this.gameOverScene.setVisible(true);
    }


    setupBlobs() {
        this.blobs = [];
        this.numberBlobs = 6;
        this.speedBlob = 2;
        let spacing = 48,
            xOffset = 150,
            direction = 1;
        for (let i = 0; i < this.numberBlobs; i++) {
            var blob = new Object(resources["image/treasureHunter.json"].textures["blob.png"]);
            blob.x = spacing * i + xOffset;
            blob.y = randomInt(0, this.height - blob.height);
            blob.vy = this.speedBlob * direction;
            direction *= -1;
            this.blobs.push(blob);
            this.gameScene.addChild(blob);
        }

    }

    drawTextLevel() {
        const styleLevel = new PIXI.TextStyle({
            fontFamily: "Futura",
            fontSize: 20,
            fill: "white"
        });

        this.levelText = new PIXI.Text("Level " + this.level, styleLevel);
        this.levelText.x = 150;
        this.levelText.y = 10;
        this.gameScene.addChild(this.levelText)
    }

    drawEndScene() {
        const style = new PIXI.TextStyle({
            fontFamily: "Futura",
            fontSize: 64,
            fill: "white"
        });
        this.message = new PIXI.Text("The End!", style);
        this.message.x = 120;
        this.message.y = this.height / 2 - 32;
        this.gameOverScene.addChild(this.message);
        this.gameOverScene.setVisible(false);
    }

    drawNextLevelScene() {
        const textureAgain = resources.button.texture;
        const rectangleAgain = new PIXI.Rectangle(200, 3050, 900, 900);
        textureAgain.frame = rectangleAgain;
        this.btn_again = new Object(textureAgain);
        this.btn_again.setScale(0.1, 0.1);
        this.btn_again.setPosition(100, 200);
        this.btn_again.interactive = true;
        this.btn_again.buttonMode = true;
        this.btn_again.on('pointerdown', this.onAgain.bind(this));
        this.nextLevelGameScene.addChild(this.btn_again);

        const textureNextLv = new PIXI.Texture(resources.button.texture);
        const rectangleNextLv = new PIXI.Rectangle(2100, 200, 900, 900);
        textureNextLv.frame = rectangleNextLv;
        this.btn_nextLevel = new Object(textureNextLv);
        this.btn_nextLevel.setScale(0.1, 0.1);
        this.btn_nextLevel.setPosition(300, 200);
        this.btn_nextLevel.interactive = true;
        this.btn_nextLevel.buttonMode = true;
        this.btn_nextLevel.on("pointerdown", this.onNextLv.bind(this));
        this.nextLevelGameScene.addChild(this.btn_nextLevel);
        this.nextLevelGameScene.setVisible(false);

    }

    onAgain() {
        this.nextLevelGameScene.setVisible(false);
        this.gameScene.setVisible(true);
        this.again();
        this.state = this.play;
    }

    onNextLv() {
        this.nextLevelGameScene.setVisible(false);
        this.gameScene.setVisible(true);
        this.level = this.level + 1;
        this.again();
        this.state = this.play;
    }

    again() {
        this.gameOverScene.setVisible(false);
        this.explorer.x = 68;
        this.explorer.y = this.height / 2 - this.explorer.height / 2;
        this.healthBar.outer.width = 128
        this.treasure.x = this.width - this.treasure.width - 48;
        this.treasure.y = this.height / 2 - this.treasure.height / 2;

    }

}

export default Game;