import { Sprite } from 'pixi.js'
class Object extends Sprite {
    constructor(textures) {
        super(textures);
        // this.textures = textures;
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
    }

    setSpeed(vx, vy) {
        this.vx = vx;
        this.vy = vy;
    }

    setScale(x, y) {
        this.scale.set(x, y);
    }
}

export default Object;