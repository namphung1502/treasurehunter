import Object from "./Object";
import { keyboard } from "./CommonFunction";
export default class Explorer extends Object {
    constructor(textures) {
        super(textures);
        this.left = keyboard("ArrowLeft");
        this.up = keyboard("ArrowUp");
        this.right = keyboard("ArrowRight");
        this.down = keyboard("ArrowDown");
    }

    move() {
        // arrow left press
        this.left.press = () => {
            this.vx = -5;
            if (this.down.isDown) {
                this.vy = 5;
            } else if (this.up.isDown) {
                this.vy = -5;
            }
        };


        this.left.release = () => {
            if (!this.right.isDown) {
                this.vx = 0;
            }
            if (this.down.isUp && this.up.isUp) {
                this.vy = 0;
            }
        };

        // arrow right press
        this.right.press = () => {
            this.vx = 5;
            if (this.down.isDown) {
                this.vy = 5;
            } else if (this.up.isDown) {
                this.vy = -5;
            }
        };
        this.right.release = () => {
            if (!this.left.isDown) {
                this.vx = 0;
            }
            if (this.down.isUp && this.up.isUp) {
                this.vy = 0;
            }
        };

        //Up
        this.up.press = () => {
            this.vy = -5;
            if (this.left.isDown) {
                this.vx = -5;
            } else if (this.right.isDown) {
                this.vx = 5;
            }
        };
        this.up.release = () => {
            if (!this.down.isDown) {
                this.vy = 0;
            }
            if (this.left.isUp && this.right.isUp) {
                this.vx = 0;
            }
        };

        //Down
        this.down.press = () => {
            this.vy = 5;
            if (this.left.isDown) {
                this.vx = -5;
            } else if (this.right.isDown) {
                this.vx = 5;
            }
        };
        this.down.release = () => {
            if (!this.up.isDown) {
                this.vy = 0;
            }
            if (this.left.isUp && this.right.isUp) {
                this.vx = 0;
            }
        };
    }
}