import { Container, Graphics } from "pixi.js"
export default class HealthBar extends Container {
    constructor() {
        super();
        this.innerBar = new Graphics();
        this.innerBar.beginFill(0x000000);
        this.innerBar.drawRect(0, 0, 128, 8);
        this.innerBar.endFill();
        this.addChild(this.innerBar);

        this.outerBar = new Graphics();
        this.outerBar.beginFill(0xFF3300);
        this.outerBar.drawRect(0, 0, 128, 8);
        this.outerBar.endFill();
        this.addChild(this.outerBar);

        this.outer = this.outerBar;
        this.outer.width = 128;
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
    }

    addChild(child) {
        super.addChild(child)
    }

    decreaseBlood(x) {
        this.outer.width -= x;
    }
}